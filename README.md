WEactivity
======

This is a simple [Widget](http://www.mediawiki.org/wiki/Extension:Widgets)
that displays a punchcard graph of WikiEducator editing activity using
jQuery and [D3.js](http://d3js.org/).

It is not scientific, and makes no judgements about the value or size of
the edits, only the number of them.

